#define _POSIX_C_SOURCE 200809L
#include <log.h>
#include <logger/fd.h>

#include <assert.h>
#include <unistd.h>

int main(void)
{
	int ret = -1;
	(void) ret; // Avoid unused warnings in release builds

	ret = log_fd_init(&log_global_logger, STDERR_FILENO, LOG_COLOR_AUTO);
	assert(ret == 0);

	ret = log_info("A log in main");
	assert(ret == 0);

	ret = log_flush();
	assert(ret == 0);

	ret = log_error("a log with trailing spaces \n\n\t\t\t   \t \r ");
	assert(ret == 0);

	ret = log_free();
	assert(ret == 0);

	return 0;
}
