#define _POSIX_C_SOURCE 200809L
#include <log.h>
#include <logger/fd.h>

#include <assert.h>

int main(void)
{
	int ret = -1;
	(void) ret; // Avoid unused warnings in release builds

	ret = log_info("A log in main");
	assert(ret == 0);

	ret = log_flush();
	assert(ret == 0);

	ret = log_free();
	assert(ret == 0);

	return 0;
}
