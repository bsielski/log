#define _POSIX_C_SOURCE 200809L
#include <log.h>
#include <logger/stub.h>

#include <assert.h>

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

static int call_vlog(enum log_level level, const char *format, ...)
{
	va_list ap;
	va_start(ap, format);
	int ret = log_vlog(level, format, ap);
	va_end(ap);
	return ret;
}

int main(void)
{
	int ret = -1;
	(void) ret; // Avoid unused warnings in release builds

	const struct log_stub_call calls[] = {
		{
			.type = LOG_STUB_CALL_LOG,
			.level = LOG_LEVEL_INFO,
			.message = "A log in main",
		},
		{
			.type = LOG_STUB_CALL_FLUSH,
		},
		{
			.type = LOG_STUB_CALL_LOG,
			.level = LOG_LEVEL_ERROR,
			.message = "Another log in main: 123",
		},
	};

	ret = log_stub_init(&log_global_logger, calls, ARRAY_SIZE(calls));
	assert(ret == 0);

	ret = log_info("A log in %s", __func__);
	assert(ret == 0);

	ret = log_flush();
	assert(ret == 0);

	ret = call_vlog(LOG_LEVEL_ERROR, "Another log in %s: %d", __func__, 123);
	assert(ret == 0);

	ret = log_free();
	assert(ret == 0);

	return 0;
}
