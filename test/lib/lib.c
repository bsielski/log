#define _POSIX_C_SOURCE 200809L
#include "lib.h"

#include <assert.h>
#include <log.h>

void lib_func(void)
{
	int ret = log_info("A log in a library");
	(void) ret; // Avoid unused warnings in release builds
	assert(ret == 0);
}
