#define _POSIX_C_SOURCE 200809L
#include <log.h>
#include <logger/stub.h>

#include <assert.h>

#include "lib.h"

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

int main(void)
{
	int ret = -1;
	(void) ret; // Avoid unused warnings in release builds

	const struct log_stub_call calls[] = {
		{
			.type = LOG_STUB_CALL_LOG,
			.level = LOG_LEVEL_INFO,
			.message = "A log in main",
		},
		{
			.type = LOG_STUB_CALL_LOG,
			.level = LOG_LEVEL_INFO,
			.message = "A log in a library",
		},
	};

	ret = log_stub_init(&log_global_logger, calls, ARRAY_SIZE(calls));
	assert(ret == 0);

	ret = log_info("A log in main");
	assert(ret == 0);

	lib_func();
	return 0;
}
