#define _POSIX_C_SOURCE 200809L
#include <log.h>
#include <logger/buffer.h>

#include <alloc.h>
#include <assert.h>
#include <stdio.h>

#define eprintf(...) fprintf(stderr, __VA_ARGS__)

int main(void)
{
	int ret = -1;
	(void) ret; // Avoid unused warnings in release builds

	ret = log_buffer_init(&log_global_logger, &alloc_std, 4096);
	assert(ret == 0);

	ret = log_info("A log in main");
	assert(ret == 0);

	ret = log_flush();
	assert(ret == 0);

	ret = log_error("a log with trailing spaces \n\n\t\t\t   \t \r ");
	assert(ret == 0);

	size_t i = 0;
	log_buffer_for_each (&log_global_logger, message) {
		eprintf("%s\n", message);
		i += 1;
	}
	assert(i == 2);

	ret = log_free();
	assert(ret == 0);

	return 0;
}
