#define _POSIX_C_SOURCE 200809L
#include <log.h>

#include <assert.h>
#include <stddef.h>

static int log_black_hole_log(
	struct logger_context *restrict context,
	enum log_level level,
	const char *restrict file,
	unsigned int line,
	const char *restrict function,
	const char *restrict format,
	va_list ap)
{
	assert(context == NULL);
	(void) context;
	(void) level;
	(void) file;
	(void) line;
	(void) function;
	(void) format;
	(void) ap;
	return 0;
}

static int log_black_hole_flush(struct logger_context *context)
{
	assert(context == NULL);
	(void) context;
	return 0;
}

static int log_black_hole_free(struct logger_context *context)
{
	assert(context == NULL);
	(void) context;
	return 0;
}

static const struct logger_vtable vtable = {
	.log = log_black_hole_log,
	.flush = log_black_hole_flush,
	.free = log_black_hole_free,
};

int log_black_hole_init(struct logger *logger)
{
	logger->vtable = &vtable;
	logger->context = NULL;
	return 0;
}

struct logger log_global_logger = {
	.vtable = &vtable,
	.max_level = LOG_LEVEL_MAX_STATIC,
	.context = NULL,
};

int _logger_log_unchecked(
	struct logger *restrict logger,
	enum log_level level,
	const char *restrict file,
	unsigned int line,
	const char *restrict function,
	const char *restrict format,
	...)
{
	va_list ap;
	va_start(ap, format);
	int ret = logger->vtable->log(logger->context, level, file, line, function, format, ap);
	va_end(ap);
	return ret;
}
