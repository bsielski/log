#define _POSIX_C_SOURCE 200809L
#include <config.h>
#include <log.h>
#include <logger/fd.h>

#include <errno.h>

#if !LOGGER_FD

int log_fd_init(struct logger *logger, int fd, enum log_use_color use_color)
{
	(void) logger;
	(void) fd;
	(void) use_color;
	return -ENOTSUP;
}

#else /* LOGGER_FD */

#include "common.h"
#include <limits.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

struct logger_context {
	int fd;
	bool color;
};

#define COLOR_RED          "\x1B[0;31m"
#define COLOR_YELLOW       "\x1B[0;33m"
#define COLOR_DEFAULT      "\x1B[0;39m"
#define COLOR_GRAY         "\x1B[0;90m"
#define COLOR_RED_BOLD     "\x1B[1;31m"
#define COLOR_YELLOW_BOLD  "\x1B[1;33m"
#define COLOR_DEFAULT_BOLD "\x1B[1;39m"
#define COLOR_GRAY_BOLD    "\x1B[1;90m"

static const char *log_color(enum log_level level, bool bold)
{
	if (bold) {
		switch (level) {
		case LOG_LEVEL_ERROR:
			return COLOR_RED_BOLD;
		case LOG_LEVEL_WARN:
			return COLOR_YELLOW_BOLD;
		case LOG_LEVEL_INFO:
			return COLOR_DEFAULT_BOLD;
		case LOG_LEVEL_DEBUG:
		case LOG_LEVEL_TRACE:
			return COLOR_GRAY_BOLD;
		default:
			return COLOR_DEFAULT_BOLD;
		}
	} else {
		switch (level) {
		case LOG_LEVEL_ERROR:
			return COLOR_RED;
		case LOG_LEVEL_WARN:
			return COLOR_YELLOW;
		case LOG_LEVEL_INFO:
			return COLOR_DEFAULT;
		case LOG_LEVEL_DEBUG:
		case LOG_LEVEL_TRACE:
			return COLOR_GRAY;
		default:
			return COLOR_DEFAULT;
		}
	}
}

static int log_write(int fd, const char *buf, size_t size)
{
	while (size) {
		ssize_t ret = -1;
		do {
			ret = write(fd, buf, size);
		} while ((ret < 0) && (errno == EINTR));
		if (ret < 0)
			return -errno;

		buf += ret;
		size -= ret;
	}
	return 0;
}

#if LINE_MAX < 32
#error "LINE_MAX is too short to even contain a timestamp"
#endif

static int fd_log(
	struct logger_context *restrict context,
	enum log_level level,
	const char *restrict file,
	unsigned int line,
	const char *restrict function,
	const char *restrict format,
	va_list ap)
{
	(void) function;
	if (!context || context->fd < 0) {
		return -EINVAL;
	}

	const char *log_level_str = log_level_to_str(level);
	if (!log_level_str) {
		return -EINVAL;
	}

	char buf[LINE_MAX];
	size_t buf_used = 0;

	struct timespec now;
	clock_gettime(CLOCK_REALTIME, &now);

	struct tm now_tm;
	localtime_r(&now.tv_sec, &now_tm);

	buf_used += strftime(buf + buf_used, sizeof(buf) - buf_used, "%Y-%m-%d %H:%M:%S", &now_tm);
	buf_used += snprintf(buf + buf_used, sizeof(buf) - buf_used, ".%09ld ", now.tv_nsec);

	if (context->color) {
		buf_used += snprintf(
			buf + buf_used,
			sizeof(buf) - buf_used,
			"%s%-5s " COLOR_GRAY "%s:%u:%s ",
			log_color(level, true),
			log_level_str,
			file,
			line,
			log_color(level, false));
	} else {
		buf_used += snprintf(
			buf + buf_used,
			sizeof(buf) - buf_used,
			"%-5s %s:%u: ",
			log_level_str,
			file,
			line);
	}
	if (buf_used >= sizeof(buf)) {
		buf_used = sizeof(buf) - 1;
	}

	size_t empty_message_len = buf_used;

	buf_used += vsnprintf(buf + buf_used, sizeof(buf) - buf_used, format, ap);
	if (buf_used >= sizeof(buf)) {
		buf_used = sizeof(buf) - 1;
	}

	buf_used -= strip_trailing_characters(buf, " \t\n\r");
	if (buf_used <= empty_message_len) {
		// Suppress empty logs
		return 0;
	}

	if (context->color) {
		if (buf_used >= sizeof(buf) - strlen(COLOR_DEFAULT)) {
			buf_used = sizeof(buf) - strlen(COLOR_DEFAULT) - 1;
		}
		buf_used += snprintf(buf + buf_used, sizeof(buf) - buf_used, COLOR_DEFAULT);
	}

	// This can overwrite the final null char, but that's OK since write()
	// is used to write the buffer to the file descriptor
	buf[buf_used] = '\n';
	buf_used += 1;

	return log_write(context->fd, buf, buf_used);
}

static int fd_flush(struct logger_context *context)
{
	if (!context || context->fd < 0) {
		return -EINVAL;
	}
	if (fsync(context->fd) && errno != EINVAL) {
		return -errno;
	}
	return 0;
}

static int fd_free(struct logger_context *context)
{
	close(context->fd);
	context->fd = -1;
	free(context);
	return 0;
}

static const struct logger_vtable vtable = {
	.log = fd_log,
	.flush = fd_flush,
	.free = fd_free,
};

int log_fd_init(struct logger *logger, int fd, enum log_use_color use_color)
{
	if (!logger || fd < 0) {
		return -EINVAL;
	}

	bool color = true;
	switch (use_color) {
	case LOG_COLOR_ALWAYS:
		color = true;
		break;
	case LOG_COLOR_NEVER:
		color = false;
		break;
	case LOG_COLOR_AUTO:
		if (isatty(fd)) {
			color = true;
		} else if (errno == ENOTTY) {
			color = false;
		} else {
			return -errno;
		}
		break;
	default:
		return -EINVAL;
	}

	struct logger_context *context = malloc(sizeof(*context));
	if (!context) {
		return -errno;
	}

	context->fd = fd;
	context->color = color;

	logger->vtable = &vtable;
	logger->context = context;
	return 0;
}

#endif /* LOGGER_FD */
