#define _POSIX_C_SOURCE 200809L
#include <config.h>
#include <log.h>
#include <logger/syslog.h>

#include <errno.h>

#if !LOGGER_SYSLOG

int log_syslog_init(
	struct logger *logger,
	enum log_syslog_version version,
	const char *ident,
	enum log_syslog_facility facility)
{
	(void) logger;
	(void) version;
	(void) ident;
	(void) facility;
	return -ENOTSUP;
}

#else /* LOGGER_SYSLOG */

#include "common.h"
#include <assert.h>
#include <limits.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <time.h>
#include <unistd.h>

struct logger_context {
	int fd;
	char ident[48];
	enum log_syslog_facility facility;
};

static int syslog_connect(int fd)
{
	const struct sockaddr_un addr = {
		.sun_family = AF_UNIX,
		.sun_path = "/dev/log",
	};
	while (connect(fd, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
		if (errno != EINTR)
			return -errno;
	}
	return 0;
}

static int send_handle_eintr(int sockfd, const void *buf, size_t len, int flags)
{
	while (send(sockfd, buf, len, flags) < 0) {
		if (errno != EINTR)
			return -errno;
	}
	return 0;
}

static int syslog_send(struct logger_context *context, const char *buffer, size_t length)
{
	int ret = send_handle_eintr(context->fd, buffer, length, MSG_NOSIGNAL);
	switch (ret) {
	case -ECONNREFUSED:
	case -ECONNRESET:
	case -ENOTCONN:
	case -EPIPE:
		ret = syslog_connect(context->fd);
		if (ret < 0) {
			return ret;
		}
		return send_handle_eintr(context->fd, buffer, length, MSG_NOSIGNAL);
	default:
		return ret;
	}
}

static int syslog_rfc3164_log(
	struct logger_context *restrict context,
	enum log_level level,
	const char *restrict file,
	unsigned int line,
	const char *restrict function,
	const char *restrict format,
	va_list ap)
{
	(void) file;
	(void) line;
	(void) function;

	int priority = log_level_to_syslog_priority(level);
	if (priority < 0) {
		return -EINVAL;
	}
	priority |= (int) context->facility;

	time_t now = time(NULL);
	struct tm tm;
	gmtime_r(&now, &tm);

	const char *ident = context->ident;

	pid_t pid = getpid();

	char buf[PIPE_BUF];
	size_t buf_used = 0;

	buf_used += snprintf(buf + buf_used, sizeof(buf) - buf_used, "<%d>", priority);
	buf_used += strftime(buf + buf_used, sizeof(buf) - buf_used, "%b %e %T ", &tm);
	buf_used += snprintf(buf + buf_used, sizeof(buf) - buf_used, "%s[%u] ", ident, pid);

	size_t empty_message_len = buf_used;
	buf_used += vsnprintf(buf + buf_used, sizeof(buf) - buf_used, format, ap);
	if (buf_used >= sizeof(buf)) {
		buf_used = sizeof(buf) - 1;
	}

	buf_used -= strip_trailing_characters(buf, " \t\n\r");
	if (buf_used <= empty_message_len) {
		// Suppress empty logs
		return 0;
	}

	return syslog_send(context, buf, buf_used);
}

static int syslog_rfc5424_log(
	struct logger_context *restrict context,
	enum log_level level,
	const char *restrict file,
	unsigned int line,
	const char *restrict function,
	const char *restrict format,
	va_list ap)
{
	int priority = log_level_to_syslog_priority(level);
	if (priority < 0) {
		return -EINVAL;
	}
	priority |= context->facility;

	pid_t pid = getpid();

	char buf[PIPE_BUF];
	size_t buf_used = 0;

	buf_used += snprintf(
		buf + buf_used,
		sizeof(buf) - buf_used,
		"<%d>1 - - %s %u - [code file=%s line=%u function=%s] ",
		priority,
		context->ident,
		pid,
		file,
		line,
		function);

	size_t empty_message_len = buf_used;
	buf_used += vsnprintf(buf + buf_used, sizeof(buf) - buf_used, format, ap);
	if (buf_used >= sizeof(buf)) {
		buf_used = sizeof(buf) - 1;
	}

	buf_used -= strip_trailing_characters(buf, " \t\n\r");
	if (buf_used <= empty_message_len) {
		// Suppress empty logs
		return 0;
	}

	return syslog_send(context, buf, buf_used);
}

static int syslog_flush(struct logger_context *context)
{
	(void) context;
	return 0;
}

static int syslog_free(struct logger_context *context)
{
	close(context->fd);
	context->fd = -1;
	free(context);
	return 0;
}

static const struct logger_vtable vtable_rfc3164 = {
	.log = syslog_rfc3164_log,
	.flush = syslog_flush,
	.free = syslog_free,
};

static const struct logger_vtable vtable_rfc5424 = {
	.log = syslog_rfc5424_log,
	.flush = syslog_flush,
	.free = syslog_free,
};

int log_syslog_init(
	struct logger *logger,
	enum log_syslog_version version,
	const char *ident,
	enum log_syslog_facility facility)
{
	if (!logger) {
		return -EINVAL;
	}

	const struct logger_vtable *vtable = NULL;
	switch (version) {
	case LOG_SYSLOG_RFC3164:
		vtable = &vtable_rfc3164;
		break;
	case LOG_SYSLOG_RFC5424:
		vtable = &vtable_rfc5424;
		break;
	default:
		return -EINVAL;
	}

	struct logger_context *context = malloc(sizeof(*context));
	if (!context) {
		return -errno;
	}

	context->facility = facility;

	context->ident[0] = '-';
	context->ident[1] = '\0';

	context->fd = socket(AF_UNIX, SOCK_DGRAM, 0);
	if (context->fd < 0) {
		int saved_errno = errno;
		free(context);
		return -saved_errno;
	}

	int ret = syslog_connect(context->fd);
	if (ret < 0) {
		free(context);
		return ret;
	}

	if (ident && ident[0] != '\0') {
		strncpy(context->ident, ident, sizeof(context->ident));
		context->ident[sizeof(context->ident) - 1] = '\0';
	}

	logger->vtable = vtable;
	logger->context = context;
	return 0;
}

#endif /* LOGGER_SYSLOG */
