#define _POSIX_C_SOURCE 200809L
#include <config.h>
#include <log.h>
#include <logger/stub.h>

#include <errno.h>

#if !LOGGER_STUB

int log_stub_init(struct logger *logger, const struct log_stub_call *calls, size_t num_calls)
{
	(void) logger;
	(void) calls;
	(void) num_calls;
	return -ENOTSUP;
}

#else /* LOGGER_STUB */

#undef NDEBUG
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct logger_context {
	const struct log_stub_call *calls;
	size_t num_calls;
	size_t received_calls;
};

static void assert_call(
	struct logger_context *restrict context,
	enum log_stub_call_type type,
	enum log_level level,
	const char *restrict format,
	va_list ap)
{
	const struct log_stub_call *expected = &context->calls[context->received_calls];
	assert(expected->type == type);
	assert(expected->level == level);

	char buf[4096];
	size_t buf_used = vsnprintf(buf, sizeof(buf), format, ap);
	assert(buf_used < sizeof(buf));
	assert(strncmp(buf, expected->message, buf_used) == 0);
}

static int stub_log(
	struct logger_context *restrict context,
	enum log_level level,
	const char *restrict file,
	unsigned int line,
	const char *restrict function,
	const char *restrict format,
	va_list ap)
{
	assert(file != NULL);
	assert(line != 0);
	assert(function != NULL);

	assert_call(context, LOG_STUB_CALL_LOG, level, format, ap);
	context->received_calls += 1;
	return 0;
}

static int stub_flush(struct logger_context *context)
{
	assert(context->calls[context->received_calls].type == LOG_STUB_CALL_FLUSH);
	context->received_calls += 1;
	return 0;
}

static int stub_free(struct logger_context *context)
{
	assert(context->received_calls == context->num_calls);
	free(context);
	return 0;
}

static const struct logger_vtable vtable = {
	.log = stub_log,
	.flush = stub_flush,
	.free = stub_free,
};

int log_stub_init(struct logger *logger, const struct log_stub_call *calls, size_t num_calls)
{
	if (!logger || (num_calls != 0 && !calls)) {
		return -EINVAL;
	}

	struct logger_context *context = malloc(sizeof(*context));
	assert(context);
	context->calls = calls;
	context->num_calls = num_calls;
	context->received_calls = 0;

	logger->vtable = &vtable;
	logger->context = context;
	return 0;
}

#endif /* LOGGER_STUB */
