#define _POSIX_C_SOURCE 200809L
#include <config.h>
#include <log.h>
#include <logger/journald.h>

#include <errno.h>

#if !LOGGER_JOURNALD

int log_journald_init(struct logger *logger, int flags)
{
	(void) logger;
	(void) flags;
	return -ENOTSUP;
}

#else /* LOGGER_JOURNALD */

#include "common.h"
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>

#define SD_JOURNAL_SUPPRESS_LOCATION
#include <systemd/sd-journal.h>

static bool stderr_is_journald(void)
{
	const char *env = getenv("JOURNAL_STREAM");
	if (!env) {
		return false;
	}

	char *end = NULL;
	unsigned long long dev = strtoull(env, &end, 10);
	if (env == end || *end != ':' || (dev == ULLONG_MAX && errno == ERANGE)) {
		return false;
	}
	env = end + 1;

	end = NULL;
	unsigned long long ino = strtoull(env, &end, 10);
	if (env == end || *end != '\0' || (ino == ULLONG_MAX && errno == ERANGE)) {
		return false;
	}

	struct stat st = {0};
	if (fstat(STDERR_FILENO, &st) < 0) {
		return false;
	}
	unsigned long long st_dev = st.st_dev;
	unsigned long long st_ino = st.st_ino;

	return st_dev == dev && st_ino == ino;
}

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

static int journald_log(
	struct logger_context *restrict context,
	enum log_level level,
	const char *restrict file,
	unsigned int line,
	const char *restrict function,
	const char *restrict format,
	va_list ap)
{
	assert(context == NULL);
	(void) context;

	int syslog_priority = log_level_to_syslog_priority(level);
	if (syslog_priority < 0) {
		return -EINVAL;
	}

	char message[LINE_MAX] = "MESSAGE=";
	size_t message_len = 8;
	size_t empty_message_len = message_len;

	message_len += vsnprintf(message + message_len, sizeof(message) - message_len, format, ap);
	if (message_len >= sizeof(message)) {
		message_len = sizeof(message) - 1;
	}

	message_len -= strip_trailing_characters(message, " \t\n\r");
	if (message_len <= empty_message_len) {
		// Suppress empty logs
		return 0;
	}

	char priority[32];
	size_t priority_len = snprintf(priority, sizeof(priority), "PRIORITY=%d", syslog_priority);
	if (priority_len >= sizeof(priority)) {
		priority_len = sizeof(priority) - 1;
	}

	char code_file[PATH_MAX];
	size_t code_file_len = snprintf(code_file, sizeof(code_file), "CODE_FILE=%s", file);
	if (code_file_len >= sizeof(code_file)) {
		code_file_len = sizeof(code_file) - 1;
	}

	char code_line[32];
	size_t code_line_len = snprintf(code_line, sizeof(code_line), "CODE_LINE=%u", line);
	if (code_line_len >= sizeof(code_line)) {
		code_line_len = sizeof(code_line) - 1;
	}

	char code_func[1024];
	size_t code_func_len = snprintf(code_func, sizeof(code_func), "CODE_FUNC=%s", function);
	if (code_func_len >= sizeof(code_func)) {
		code_func_len = sizeof(code_func) - 1;
	}

	const struct iovec iov[] = {
		{
			.iov_base = priority,
			.iov_len = priority_len,
		},
		{
			.iov_base = code_file,
			.iov_len = code_file_len,
		},
		{
			.iov_base = code_line,
			.iov_len = code_line_len,
		},
		{
			.iov_base = code_func,
			.iov_len = code_func_len,
		},
		{
			.iov_base = message,
			.iov_len = message_len,
		},
	};
	return sd_journal_sendv(iov, ARRAY_SIZE(iov));
}

static int journald_flush(struct logger_context *context)
{
	assert(context == NULL);
	(void) context;
	return 0;
}

static int journald_free(struct logger_context *context)
{
	assert(context == NULL);
	(void) context;
	return 0;
}

static const struct logger_vtable vtable = {
	.log = journald_log,
	.flush = journald_flush,
	.free = journald_free,
};

int log_journald_init(struct logger *logger, int flags)
{
	if (!logger) {
		return -EINVAL;
	}

	if (!(flags & LOG_JOURNALD_SKIP_STDERR_CHECK) && !stderr_is_journald()) {
		return -EPROTOTYPE;
	}

	logger->vtable = &vtable;
	logger->context = NULL;
	return 0;
}

#endif /* LOGGER_JOURNALD */
