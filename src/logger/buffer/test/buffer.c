#define _POSIX_C_SOURCE 200809L
#include "buffer.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#define eprintf(...) fprintf(stderr, __VA_ARGS__)

static int streq(const char *a, const char *b)
{
	return strcmp(a, b) == 0;
}

static void dump_buffer(const struct buffer *buffer)
{
	size_t start_offset = buffer->start - buffer->data;
	size_t end_offset = buffer->end - buffer->data;

	for (size_t i = 0; i < buffer->size; ++i) {
		if (i % 16 == 0) {
			if (i)
				eprintf("\n");
			eprintf("# ");
		}

		unsigned char c = buffer->data[i];

		if (i == start_offset && i == end_offset) {
			eprintf("%02Xb ", c);
		} else if (i == start_offset) {
			eprintf("%02Xs ", c);
		} else if (i == end_offset) {
			eprintf("%02Xe ", c);
		} else {
			eprintf("%02X  ", c);
		}
	}
	eprintf("\n\n");
}

#define SIZE 32

static void test_add(void)
{
	char data[SIZE];
	memset(data, 0xFE, sizeof(data));

	struct buffer buffer = {
		.data = data,
		.size = SIZE,
		.start = NULL,
		.end = data,
	};

	buffer_add_str(&buffer, "abc", 3);
	assert(buffer.start == buffer.data);
	assert(buffer.end == buffer.data + 4);

	buffer_add_str(&buffer, "defghi", 6);
	assert(buffer.start == buffer.data);
	assert(buffer.end == buffer.data + 11);

	buffer_add_str(&buffer, "jklmnopqrstuvwxyz", 17);
	assert(buffer.start == buffer.data);
	assert(buffer.end == buffer.data + 29);

	buffer_add_str(&buffer, "", 0);
	assert(buffer.start == buffer.data);
	assert(buffer.end == buffer.data + 30);

	buffer_add_str(&buffer, "", 0);
	assert(buffer.start == buffer.data);
	assert(buffer.end == buffer.data + 31);

	buffer_add_str(&buffer, "", 0);
	assert(buffer.start == buffer.data);
	assert(buffer.end == buffer.data);

	size_t i = 0;
	buffer_for_each (&buffer, str) {
		switch (i) {
		case 0:
			assert(streq(str, "abc"));
			break;
		case 1:
			assert(streq(str, "defghi"));
			break;
		case 2:
			assert(streq(str, "jklmnopqrstuvwxyz"));
			break;
		case 3:
			assert(streq(str, ""));
			break;
		case 4:
			assert(streq(str, ""));
			break;
		case 5:
			assert(streq(str, ""));
			break;
		}
		++i;
	}
	assert(i == 6);
}

static void test_rollover_1(void)
{
	char data[SIZE];
	memset(data, 0xFE, sizeof(data));

	struct buffer buffer = {
		.data = data,
		.size = SIZE,
		.start = NULL,
		.end = data,
	};

	buffer_add_str(&buffer, "abcdefg", 7);
	assert(buffer.start == buffer.data);
	assert(buffer.end == buffer.data + 8);

	buffer_add_str(&buffer, "hijklmn", 7);
	assert(buffer.start == buffer.data);
	assert(buffer.end == buffer.data + 16);

	buffer_add_str(&buffer, "opqrstuvwxyz", 12);
	dump_buffer(&buffer);
	assert(buffer.start == buffer.data);
	assert(buffer.end == buffer.data + 29);

	buffer_add_str(&buffer, "ABCDE", 5);
	dump_buffer(&buffer);
	assert(buffer.start == buffer.data + 8);
	assert(buffer.end == buffer.data + 6);

	size_t i = 0;
	buffer_for_each (&buffer, str) {
		switch (i) {
		case 0:
			assert(streq(str, "hijklmn"));
			break;
		case 1:
			assert(streq(str, "opqrstuvwxyz"));
			break;
		case 2:
			assert(streq(str, "ABCDE"));
			break;
		}
		++i;
	}
	assert(i == 3);

	buffer_add_str(&buffer, "abcdefghijklmnopqrstuvwxy", 25);
	dump_buffer(&buffer);
	assert(buffer.start == buffer.data);
	assert(buffer.end == buffer.data);

	i = 0;
	buffer_for_each (&buffer, str) {
		switch (i) {
		case 0:
			assert(streq(str, "ABCDE"));
			break;
		case 1:
			assert(streq(str, "abcdefghijklmnopqrstuvwxy"));
			break;
		}
		++i;
	}
	assert(i == 2);
}

static void test_fill_exact(void)
{
	char data[SIZE];
	memset(data, 0xFE, sizeof(data));

	struct buffer buffer = {
		.data = data,
		.size = SIZE,
		.start = NULL,
		.end = data,
	};

	buffer_add_str(&buffer, "1234567890123456789012345678901", 31);
	assert(buffer.start == buffer.data);
	assert(buffer.end == buffer.data);

	size_t i = 0;
	buffer_for_each (&buffer, str) {
		switch (i) {
		case 0:
			assert(streq(str, "1234567890123456789012345678901"));
			break;
		}
		++i;
	}
	assert(i == 1);

	buffer_add_str(&buffer, "abcdefghijklmnopqrstuvwxyzABCDE", 31);
	assert(buffer.start == buffer.data);
	assert(buffer.end == buffer.data);

	i = 0;
	buffer_for_each (&buffer, str) {
		switch (i) {
		case 0:
			assert(streq(str, "abcdefghijklmnopqrstuvwxyzABCDE"));
			break;
		}
		++i;
	}
	assert(i == 1);
}

int main(void)
{
	test_add();
	test_fill_exact();
	test_rollover_1();
	return 0;
}
