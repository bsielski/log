#include <alloc.h>
#include <errno.h>
#include <stddef.h>
#include <string.h>

struct buffer {
	char *data;
	size_t size;
	char *start;
	char *end;
};

void buffer_add_str(struct buffer *restrict buffer, const char *restrict str, size_t len);

const char *buffer_next_str(const struct buffer *restrict buffer, const char *restrict str);

#define buffer_for_each(buffer_ptr, iter_var)                                                      \
	for (const char *iter_var = (buffer_ptr)->start; iter_var != NULL;                         \
	     iter_var = buffer_next_str((buffer_ptr), iter_var))
