#define _POSIX_C_SOURCE 200809L
#include "buffer.h"

#include <assert.h>
#include <string.h>

static const char *find_next_start(const struct buffer *restrict buffer, const char *restrict str)
{
	assert(buffer->data <= str && str < buffer->data + buffer->size);

	size_t max_len = (buffer->data + buffer->size - 1) - str;
	size_t len = strnlen(str, max_len);

	if (len == max_len) {
		str = buffer->data;
	} else {
		str = str + len + 1;
		if (str != buffer->end && (unsigned char) *str == 0xFF) {
			str = buffer->data;
		}
	}
	return str;
}

void buffer_add_str(struct buffer *restrict buffer, const char *restrict str, size_t len)
{
	if (len >= buffer->size) {
		// The new string doesn't fit in the buffer
		return;
	}
	if (len + 1 == buffer->size) {
		// The new string exactly fills the buffer
		strncpy(buffer->data, str, len);
		buffer->data[len] = '\0';
		buffer->start = buffer->data;
		buffer->end = buffer->data;
		return;
	}

	if (buffer->end + len + 1 > buffer->data + buffer->size) {
		// The new string doesn't fit at the end of the buffer =>
		// mark the end as invalid and move the end pointer to the beginning of the buffer
		size_t bytes_at_end = (buffer->data + buffer->size) - buffer->end;
		memset(buffer->end, 0xFF, bytes_at_end);
		buffer->end = buffer->data;
	}

	int check_start = buffer->start && buffer->end <= buffer->start;

	strncpy(buffer->end, str, len);
	buffer->end[len] = '\0';
	buffer->end = buffer->end + len + 1;
	if (buffer->end == buffer->data + buffer->size) {
		// The buffer is exactly full
		buffer->start = buffer->data;
		buffer->end = buffer->data;
		return;
	}

	if (check_start && buffer->start <= buffer->end) {
		buffer->start = (char *) find_next_start(buffer, buffer->end);
	} else if (!buffer->start) {
		buffer->start = buffer->data;
	}
}

const char *buffer_next_str(const struct buffer *restrict buffer, const char *restrict str)
{
	if (str == NULL) {
		return buffer->start;
	}
	str = find_next_start(buffer, str);
	return str != buffer->end ? str : NULL;
}
