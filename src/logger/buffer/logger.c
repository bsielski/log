#define _POSIX_C_SOURCE 200809L
#include <log.h>
#include <logger/buffer.h>

#include "../common.h"
#include "buffer.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

struct logger_context {
	const struct alloc *allocator;
	struct buffer buffer;
};

static int buffer_log(
	struct logger_context *restrict context,
	enum log_level level,
	const char *restrict file,
	unsigned int line,
	const char *restrict function,
	const char *restrict format,
	va_list ap)
{
	(void) function;

	const char *log_level_str = log_level_to_str(level);
	if (!log_level_str) {
		return -EINVAL;
	}

	char buf[1024];
	size_t buf_used = 0;

	struct timespec now;
	clock_gettime(CLOCK_REALTIME, &now);

	struct tm now_tm;
	localtime_r(&now.tv_sec, &now_tm);

	buf_used += strftime(buf + buf_used, sizeof(buf) - buf_used, "%Y-%m-%d %H:%M:%S", &now_tm);
	buf_used += snprintf(buf + buf_used, sizeof(buf) - buf_used, ".%09ld ", now.tv_nsec);

	buf_used += snprintf(
		buf + buf_used, sizeof(buf) - buf_used, "%-5s %s:%u: ", log_level_str, file, line);
	if (buf_used >= sizeof(buf)) {
		buf_used = sizeof(buf) - 1;
	}

	size_t empty_message_len = buf_used;

	buf_used += vsnprintf(buf + buf_used, sizeof(buf) - buf_used, format, ap);
	if (buf_used >= sizeof(buf)) {
		buf_used = sizeof(buf) - 1;
	}

	buf_used -= strip_trailing_characters(buf, " \t\n\r");
	if (buf_used <= empty_message_len) {
		// Suppress empty logs
		return 0;
	}

	buffer_add_str(&context->buffer, buf, buf_used);
	return 0;
}

static int buffer_flush(struct logger_context *context)
{
	(void) context;
	return 0;
}

static int buffer_free(struct logger_context *context)
{
	alloc_dealloc(context->allocator, context->buffer.data);
	alloc_dealloc(context->allocator, context);
	return 0;
}

static const struct logger_vtable vtable = {
	.log = buffer_log,
	.flush = buffer_flush,
	.free = buffer_free,
};

int log_buffer_init(struct logger *logger, const struct alloc *allocator, size_t size)
{
	struct logger_context *context = alloc_alloc(allocator, sizeof(*context));
	if (!context) {
		return -errno;
	}
	context->allocator = allocator;

	char *data = alloc_alloc(allocator, size);
	if (!data) {
		int saved_errno = errno;
		alloc_dealloc(allocator, context);
		return -saved_errno;
	}
	context->buffer.data = data;
	context->buffer.size = size;
	context->buffer.start = NULL;
	context->buffer.end = data;

	logger->vtable = &vtable;
	logger->context = context;
	return 0;
}

const char *log_buffer_next_message(const struct logger *logger, const char *current_message)
{
	return buffer_next_str(&logger->context->buffer, current_message);
}
