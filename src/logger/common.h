#include <log.h>

#include <stddef.h>

static inline int is_bad_char(const char *bad_chars, char c)
{
	for (; *bad_chars && c != *bad_chars; ++bad_chars) {}
	return *bad_chars != '\0';
}

static inline size_t strip_trailing_characters(char *str, const char *bad_chars)
{
	char *c = str;
	for (; *c != '\0'; c++) {
		if (!is_bad_char(bad_chars, *c)) {
			str = c;
		}
	}
	str[1] = '\0';
	return c - (str + 1);
}

static inline const char *log_level_to_str(enum log_level level)
{
	switch (level) {
	case LOG_LEVEL_ERROR:
		return "ERROR";
	case LOG_LEVEL_WARN:
		return "WARN";
	case LOG_LEVEL_INFO:
		return "INFO";
	case LOG_LEVEL_DEBUG:
		return "DEBUG";
	case LOG_LEVEL_TRACE:
		return "TRACE";
	default:
		return NULL;
	}
}

static inline int log_level_to_syslog_priority(enum log_level level)
{
	switch (level) {
	case LOG_LEVEL_ERROR:
		return 3; // LOG_ERR
	case LOG_LEVEL_WARN:
		return 4; // LOG_WARNING
	case LOG_LEVEL_INFO:
		return 6; // LOG_INFO
	case LOG_LEVEL_DEBUG:
		return 7; // LOG_DEBUG
	case LOG_LEVEL_TRACE:
		return 7; // LOG_DEBUG
	default:
		return -1;
	}
}
