#pragma once
#include <_log_common.h>

enum log_use_color {
	LOG_COLOR_AUTO,
	LOG_COLOR_ALWAYS,
	LOG_COLOR_NEVER,
};

LOG_PUBLIC
int log_fd_init(struct logger *logger, int fd, enum log_use_color use_color);
