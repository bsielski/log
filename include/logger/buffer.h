#pragma once
#include <_log_common.h>

#include <alloc.h>
#include <stddef.h>

LOG_PUBLIC
int log_buffer_init(struct logger *logger, const struct alloc *allocator, size_t size);

LOG_PUBLIC
const char *log_buffer_next_message(const struct logger *logger, const char *current_message);

#define log_buffer_for_each(logger, iter_variable)                                                 \
	for (const char *iter_variable = log_buffer_next_message((logger), NULL);                  \
	     iter_variable != NULL;                                                                \
	     iter_variable = log_buffer_next_message((logger), iter_variable))
