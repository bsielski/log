#pragma once
#include <_log_common.h>

#include <stddef.h>

enum log_stub_call_type {
	LOG_STUB_CALL_LOG,
	LOG_STUB_CALL_VLOG,
	LOG_STUB_CALL_FLUSH,
};

struct log_stub_call {
	enum log_stub_call_type type;
	enum log_level level;
	const char *message;
};

LOG_PUBLIC
int log_stub_init(struct logger *logger, const struct log_stub_call *calls, size_t num_calls);
