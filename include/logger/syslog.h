#pragma once
#include <_log_common.h>

enum log_syslog_version {
	LOG_SYSLOG_RFC3164,
	LOG_SYSLOG_RFC5424,
};

enum log_syslog_facility {
	LOG_SYSLOG_KERN = 0 << 3,
	LOG_SYSLOG_USER = 1 << 3,
	LOG_SYSLOG_MAIL = 2 << 3,
	LOG_SYSLOG_DAEMON = 3 << 3,
	LOG_SYSLOG_AUTH = 4 << 3,
	LOG_SYSLOG_SYSLOG = 5 << 3,
	LOG_SYSLOG_LPR = 6 << 3,
	LOG_SYSLOG_NEWS = 7 << 3,
	LOG_SYSLOG_UUCP = 8 << 3,
	LOG_SYSLOG_CRON = 9 << 3,
	LOG_SYSLOG_AUTHPRIV = 10 << 3,
	LOG_SYSLOG_FTP = 11 << 3,

	LOG_SYSLOG_LOCAL0 = 16 << 3,
	LOG_SYSLOG_LOCAL1 = 17 << 3,
	LOG_SYSLOG_LOCAL2 = 18 << 3,
	LOG_SYSLOG_LOCAL3 = 19 << 3,
	LOG_SYSLOG_LOCAL4 = 20 << 3,
	LOG_SYSLOG_LOCAL5 = 21 << 3,
	LOG_SYSLOG_LOCAL6 = 22 << 3,
	LOG_SYSLOG_LOCAL7 = 23 << 3,
};

LOG_PUBLIC int log_syslog_init(
	struct logger *logger,
	enum log_syslog_version version,
	const char *ident,
	enum log_syslog_facility facility);
