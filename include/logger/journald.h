#pragma once
#include <_log_common.h>

enum {
	LOG_JOURNALD_SKIP_STDERR_CHECK = LOG_BIT(0),
};

LOG_PUBLIC
int log_journald_init(struct logger *logger, int flags);
