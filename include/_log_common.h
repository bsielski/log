#pragma once
#if defined _WIN32 || defined __CYGWIN__
#ifdef BUILDING_LOG
#define LOG_PUBLIC __declspec(dllexport)
#else
#define LOG_PUBLIC __declspec(dllimport)
#endif
#else
#ifdef BUILDING_LOG
#define LOG_PUBLIC __attribute__((visibility("default")))
#else
#define LOG_PUBLIC
#endif
#endif

#define LOG_BIT(n) (1 << (n))

#ifdef __cplusplus
extern "C" {
#endif

struct logger_context;

struct logger_vtable;

struct logger;

enum log_level {
	LOG_LEVEL_DISABLED,
	LOG_LEVEL_ERROR,
	LOG_LEVEL_WARN,
	LOG_LEVEL_INFO,
	LOG_LEVEL_DEBUG,
	LOG_LEVEL_TRACE,
};

#ifdef __cplusplus
} // extern "C"
#endif
