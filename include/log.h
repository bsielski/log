#pragma once
#include <errno.h>
#include <stdarg.h>

#include <_log_common.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef LOG_LEVEL_MAX_STATIC
#define LOG_LEVEL_MAX_STATIC LOG_LEVEL_TRACE
#endif

struct logger_context;

struct logger_vtable {
	int (*log)(
		struct logger_context *restrict context,
		enum log_level level,
		const char *restrict file,
		unsigned int line,
		const char *restrict function,
		const char *restrict format,
		va_list ap);

	int (*flush)(struct logger_context *context);

	int (*free)(struct logger_context *context);
};

struct logger {
	const struct logger_vtable *vtable;
	enum log_level max_level;
	struct logger_context *context;
};

static inline int logger_flush(const struct logger *logger)
{
	return logger->vtable->flush(logger->context);
}

static inline int logger_free(const struct logger *logger)
{
	return logger->vtable->free(logger->context);
}

static inline enum log_level logger_max_level(const struct logger *logger)
{
	return logger->max_level;
}

static inline int logger_set_max_level(struct logger *logger, enum log_level level)
{
	if (level > LOG_LEVEL_MAX_STATIC || level <= LOG_LEVEL_DISABLED) {
		return -EINVAL;
	}
	logger->max_level = level;
	return 0;
}

static inline int logger_level_enabled(const struct logger *logger, enum log_level level)
{
	return level > LOG_LEVEL_DISABLED && level < LOG_LEVEL_MAX_STATIC
	       && level < logger_max_level(logger);
}

LOG_PUBLIC
int _logger_log_unchecked(
	struct logger *restrict logger,
	enum log_level level,
	const char *restrict file,
	unsigned int line,
	const char *restrict function,
	const char *restrict format,
	...);

#define logger_log_unchecked(logger, level, ...)                                                   \
	_logger_log_unchecked((logger), (level), __FILE__, __LINE__, __func__, __VA_ARGS__)

#define logger_log(logger, level, ...)                                                             \
	logger_level_enabled((logger), (level))                                                    \
		? logger_log_unchecked((logger), (level), __VA_ARGS__)                             \
		: 0

#define logger_error(logger, ...) logger_log((logger), LOG_LEVEL_ERROR, __VA_ARGS__)
#define logger_warn(logger, ...)  logger_log((logger), LOG_LEVEL_WARN, __VA_ARGS__)
#define logger_info(logger, ...)  logger_log((logger), LOG_LEVEL_INFO, __VA_ARGS__)
#define logger_debug(logger, ...) logger_log((logger), LOG_LEVEL_DEBUG, __VA_ARGS__)
#define logger_trace(logger, ...) logger_log((logger), LOG_LEVEL_TRACE, __VA_ARGS__)

#define logger_vlog_unchecked(logger, level, fmt, ap)                                              \
	(logger)->vtable->log((logger)->context, (level), __FILE__, __LINE__, __func__, (fmt), (ap))

#define logger_vlog(logger, level, fmt, ap)                                                        \
	logger_level_enabled((logger), (level))                                                    \
		? logger_vlog_unchecked((logger), (level), (fmt), (ap))                            \
		: 0

#define logger_verror(logger, fmt, ap) logger_vlog((logger), LOG_LEVEL_ERROR, (fmt), (ap))
#define logger_vwarn(logger, fmt, ap)  logger_vlog((logger), LOG_LEVEL_WARN, (fmt), (ap))
#define logger_vinfo(logger, fmt, ap)  logger_vlog((logger), LOG_LEVEL_INFO, (fmt), (ap))
#define logger_vdebug(logger, fmt, ap) logger_vlog((logger), LOG_LEVEL_DEBUG, (fmt), (ap))
#define logger_vtrace(logger, fmt, ap) logger_vlog((logger), LOG_LEVEL_TRACE, (fmt), (ap))

LOG_PUBLIC
extern struct logger log_global_logger;

LOG_PUBLIC
int log_black_hole_init(struct logger *logger);

static inline int log_flush(void)
{
	return logger_flush(&log_global_logger);
}

static inline int log_free(void)
{
	return logger_free(&log_global_logger);
}

static inline enum log_level log_max_level(void)
{
	return logger_max_level(&log_global_logger);
}

static inline void log_set_max_level(enum log_level level)
{
	logger_set_max_level(&log_global_logger, level);
}

static inline int log_level_enabled(enum log_level level)
{
	return logger_level_enabled(&log_global_logger, level);
}

#define log_log(level, ...) logger_log(&log_global_logger, (level), __VA_ARGS__)

#define log_error(...) logger_error(&log_global_logger, __VA_ARGS__)
#define log_warn(...)  logger_warn(&log_global_logger, __VA_ARGS__)
#define log_info(...)  logger_info(&log_global_logger, __VA_ARGS__)
#define log_debug(...) logger_debug(&log_global_logger, __VA_ARGS__)
#define log_trace(...) logger_trace(&log_global_logger, __VA_ARGS__)

#define log_vlog(level, format, ap) logger_vlog(&log_global_logger, (level), (format), (ap))

#define log_verror(fmt, ap) logger_verror(&log_global_logger, (fmt), (ap))
#define log_vwarn(fmt, ap)  logger_vwarn(&log_global_logger, (fmt), (ap))
#define log_vinfo(fmt, ap)  logger_vinfo(&log_global_logger, (fmt), (ap))
#define log_vdebug(fmt, ap) logger_vdebug(&log_global_logger, (fmt), (ap))
#define log_vtrace(fmt, ap) logger_vtrace(&log_global_logger, (fmt), (ap))

#ifdef __cplusplus
} // extern "C"
#endif
